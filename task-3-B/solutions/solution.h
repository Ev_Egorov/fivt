#pragma once

#include <functional>
#include <future>

template <class T>
class ThreadPool {
 public:
  ThreadPool();

  explicit ThreadPool(const size_t num_threads);

  std::future<T> Submit(std::function<T()> task);

  void Shutdown();

  ~ThreadPool();
};
