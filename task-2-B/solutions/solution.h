#pragma once

#include <cstddef>
#include <iostream>

class Robot {
public:
    void StepLeft() {
        std::cout << "left" << std::endl;
    }

    void StepRight() {
        std::cout << "right" << std::endl;
    }
};