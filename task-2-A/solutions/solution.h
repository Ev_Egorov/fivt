#pragma once

#include <condition_variable>
#include <cstddef>

template <typename ConditionVariable = std::condition_variable>
class CyclicBarrier {
public:
    CyclicBarrier(size_t num_threads);

    void Pass();
};
