#pragma once

#include <functional>
#include <thread>
#include <vector>

/////////////////////////////////////////////////////////////////////

// simple executor that executes each task in separate thread
class TaskExecutor {
public:
    void Execute(std::function<void()> task) {
        worker_threads_.emplace_back(task);
    }

    ~TaskExecutor() {
        for (auto& worker : worker_threads_) {
            worker.join();
        }
    }

private:
    std::vector<std::thread> worker_threads_;
};

/////////////////////////////////////////////////////////////////////
