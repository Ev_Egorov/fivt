#pragma once

#include <functional>
#include <vector>

///////////////////////////////////////////////////////////////////////

template <typename T, class Hash = std::hash<T>>
class StripedHashSet {
 public:
    explicit StripedHashSet(const size_t concurrency_level,
                            const size_t growth_factor = 3,
                            const double load_factor = 0.75) {
    }

    bool Insert(const T& element) {
        return false; // not implemented
    }

    bool Remove(const T& element) {
        return false; // not implemented
    }

    bool Contains(const T& element) {
        return false; // not implemented
    }

    size_t Size() {
        return 0;
    }

 private:
    size_t GetBucketIndex(const size_t element_hash_value) const {
        return -1; // not implemented
    }

    size_t GetStripeIndex(const size_t element_hash_value) const {
        return -1; // not implemented
    }

 private:
    // ...
};

template <typename T> using ConcurrentSet = StripedHashSet<T>;

///////////////////////////////////////////////////////////////////////
